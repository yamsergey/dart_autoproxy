import 'package:dart_autoproxy/dart_autoproxy.dart' as dart_autoproxy;

void main(List<String> arguments) {
  print('Hello world: ${dart_autoproxy.calculate()}!');
}
