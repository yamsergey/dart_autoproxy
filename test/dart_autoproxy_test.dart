import 'package:dart_autoproxy/dart_autoproxy.dart';
import 'package:test/test.dart';

void main() {
  test('calculate', () {
    expect(calculate(), 42);
  });
}
